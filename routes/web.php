<?php

use App\Http\Controllers\DataController;
use App\Http\Controllers\TrafoController;
use App\Http\Controllers\DataPostController;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'prefix'=>config('admin.prefix'),
    'namespace'=>'App\\Http\\Controllers',
], function () {

    Route::get('login','LoginAdminController@formLogin')->name('admin.login');
    Route::post('login','LoginAdminController@login');

    Route::middleware(['auth:admin'])->group(function () {
    Route::post('logout','LoginAdminController@logout')->name('admin.logout');
    Route::get('/',[DataController::class, "dashboard"])->name('dashboard');
    Route::view('/post','data-post')->name('post')->middleware('can:role,"editor"');
    Route::post('/post',[DataController::class,'cariData'])->name('cariData')->middleware('can:role,"editor"');
    Route::get('/admin',[DataController::class, "dataAdmin"])->name('admin')->middleware('can:role,"admin"');

    Route::get('/form/{id}',[DataController::class, "editData"])->name ('admin.form');
    Route::post('/form',[DataController::class, "saveData"])->name ('save.form');
    Route::view('/Detail','User.Detail')->name ('User.Detail');
    Route::view('/Pengukur','User.Pengukur')->name('User.Pengukur');

    Route::get('/Update/{id}',[DataController::class, "Update"])->name ('Update');
    Route::post('/Updatedata/{id}',[DataController::class, "Updatedata"])->name ('Pengukur');


    Route::get('/delete/{id}',[DataController::class, "delete"])->name ('dataAdmin');
});
    
});