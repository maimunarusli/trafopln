
@extends('template')

@section('content')
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Admin</title>

  
  

</head>
<body>

  <div class="container mt-3">
    <div class="row g-3 align-items-center">
    <form action="{{ route("save.form") }}" method="POST" enctype="multipart/form-data">
        @csrf
        @foreach ($dataAll["trafo"] as $item)
        @php
            $id = (int)$item->id - 1;
        @endphp     
        @endforeach
        <input type="hidden" name="alamat_id" value="{{ $dataAll["alamat"] }}">
        <input type="hidden" name="trafo_id" value="{{ $id }}">

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RA" class="col-form-label">R-A {{ $item[1] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="RA" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SA" class="col-form-label">S-A {{ $item[2] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="SA" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TA" class="col-form-label">T-A {{ $item[3] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="TA" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NA" class="col-form-label">N-A {{ $item[4] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="NA" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>


        

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RB" class="col-form-label">R-B {{ $item[5] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="RB" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SB" class="col-form-label">S-B {{ $item[6] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="SB" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TB" class="col-form-label">T-B {{ $item[7] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="TB" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NB" class="col-form-label">N-B {{ $item[7] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="NB" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RC" class="col-form-label">R-C {{ $item[9] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="RC" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SC" class="col-form-label">S-C {{ $item[10] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="SC" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TC" class="col-form-label">T-C {{ $item[11] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="TC" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NC" class="col-form-label">N-C {{ $item[12] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="NC" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RD" class="col-form-label">R-D {{ $item[13] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="RD" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SD" class="col-form-label">S-D {{ $item[14] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="SD" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TD" class="col-form-label">T-D {{ $item[15] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="TD" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="ND" class="col-form-label">N-D {{ $item[16] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="ND" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RN" class="col-form-label">RN {{ $item[21] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="RN" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SN" class="col-form-label">SN {{ $item[22] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="SN" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TN" class="col-form-label">TN {{ $item[23] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="TN" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
                <div></div>
                <div class="col">
                  <label for="formFileMultiple" class="col-form-label">Dokumentasi</label>
                </div>
                <div class="col">
                  <input type="file" name="foto" class="form-control">
                </div>
            </div>
        </div> 
    </div>
    <div class="container mt-3 , mb-3">
        <button class="btn btn-primary">SIMPAN</button>
        <button class="btn btn-danger">KEMBALI</button>
    </div>
    </form>  
  </div>
</body>


@endsection
