
@extends('template')

@section('content')
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Admin</title>
    
</head>
<body >
  <div class="container">

    <form action="{{url('Data_trafo')}}" method="POST">
      <table>
          <div class="form-group row">
            <label for="tempat" class="col-sm-1 col-form-label"><b>Tanggal </b></label>
            <div class="col-sm-3">
              <input type="Date" id="ttl" name="ttl" class="form-control" >
            </div>
          </div>
          <br>
        <tr colspan="12"><b>PHASA YANG DITAMBAH</b></tr>
        <tr align="center" >
          <th width="20px"></th>
          <th colspan="4">Line A</th>
          <th colspan="4">Line B</th>
          <th colspan="4">Line C</th>
          <th colspan="4">Line D</th>
        </tr>
        <tr>
          <th height="20px"></th>
        </tr>
        <tr align="center">
          <th width="20px"></th>
          <th>R</th>
          <th>S</th>
          <th>T</th>
          <th>N</th>
  
          <th>R</th>
          <th>S</th>
          <th>T</th>
          <th>N</th>
          
          <th>R</th>
          <th>S</th>
          <th>T</th>
          <th>N</th>
          
          <th>R</th>
          <th>S</th>
          <th>T</th>
          <th>N</th>
        </tr>
        <tr align="center">
          <th width="20px"></th>
          <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
            <div class="col-sm-3">
              <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
            </div></td>
          <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
            <div class="col-sm-3">
              <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
            </div></td>
          <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
            <div class="col-sm-3">
              <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
            </div></td>
            <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
              <div class="col-sm-3">
                <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
              </div></td>
            <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
              <div class="col-sm-3">
                <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
              </div></td>
            <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
              <div class="col-sm-3">
                <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
              </div></td>
              <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                <div class="col-sm-3">
                  <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                </div></td>
              <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                <div class="col-sm-3">
                  <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                </div></td>
              <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                <div class="col-sm-3">
                  <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                </div></td>
                <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                  <div class="col-sm-3">
                    <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                  </div></td>
                <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                  <div class="col-sm-3">
                    <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                  </div></td>
                <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                  <div class="col-sm-3">
                    <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                  </div></td>
                  <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                    <div class="col-sm-3">
                      <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                    </div></td>
                    <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                      <div class="col-sm-3">
                        <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                      </div></td>
                      <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                        <div class="col-sm-3">
                          <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                        </div></td>
                        <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                          <div class="col-sm-3">
                            <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                          </div></td>
        </tr>
      </table>
      <table>
        <br>
              <tr colspan="12"><b>PHASA YANG DIKURANGI</b></tr>
              <tr align="center" >
                <th width="20px"></th>
                <th colspan="4">Line A</th>
                <th colspan="4">Line B</th>
                <th colspan="4">Line C</th>
                <th colspan="4">Line D</th>
              </tr>
              <tr>
                <th height="20px"></th>
              </tr>
              <tr align="center">
                <th width="20px"></th>
                <th>R</th>
                <th>S</th>
                <th>T</th>
                <th>N</th>
        
                <th>R</th>
                <th>S</th>
                <th>T</th>
                <th>N</th>
                
                <th>R</th>
                <th>S</th>
                <th>T</th>
                <th>N</th>
                
                <th>R</th>
                <th>S</th>
                <th>T</th>
                <th>N</th>
              </tr>
              <tr align="center">
                <th width="20px"></th>
                <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                  <div class="col-sm-3">
                    <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                  </div></td>
                <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                  <div class="col-sm-3">
                    <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                  </div></td>
                <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                  <div class="col-sm-3">
                    <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                  </div></td>
                  <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                    <div class="col-sm-3">
                      <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                    </div></td>
                  <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                    <div class="col-sm-3">
                      <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                    </div></td>
                  <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                    <div class="col-sm-3">
                      <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                    </div></td>
                    <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                      <div class="col-sm-3">
                        <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                      </div></td>
                    <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                      <div class="col-sm-3">
                        <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                      </div></td>
                    <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                      <div class="col-sm-3">
                        <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                      </div></td>
                      <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                        <div class="col-sm-3">
                          <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                        </div></td>
                      <td width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                        <div class="col-sm-3">
                          <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                        </div></td>
                      <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                        <div class="col-sm-3">
                          <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                        </div></td>
                        <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                          <div class="col-sm-3">
                            <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                          </div></td>
                          <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                            <div class="col-sm-3">
                              <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                            </div></td>
                            <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                              <div class="col-sm-3">
                                <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                              </div></td>
                              <td  width="90px"><label for="Line" class="col-sm-1 col-form-label"></label>
                                <div class="col-sm-3">
                                  <input type="Number" id="Line" name="Line" class="form-control" style="width:70px;" >
                                </div></td>
              </tr>
            </table>
  </form>
  <br>
  <div class="form-group row">
    <label for="Tempat" class="col-sm-1 col-form-label"><b>Nomor Tiang</b></label>
    <div class="col-sm-2">
      <input type="Text" id="tiang" name="tiang" class="form-control" >
    </div>
  </div>
  <br>
      <tr>
        <td width="150px"><label for="formFileMultiple" class="col-sm-6 col-form-label"><b>Dokumentasi</b></label></td>
        <td><input class="form-control" type="file" name="pasphoto" id="pasphoto" multiple></td>
      </tr>
  <br>
  <div class="col-auto">
    <button type="submit" class="btn btn-primary mb-3">Save</button>
  </div>
  </form>
  </div>
</body>


@endsection
