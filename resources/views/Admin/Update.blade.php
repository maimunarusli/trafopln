
@extends('template')

@section('content')
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Admin</title>
</head>
<body>

  <div class="container mt-3">
    <div class="row g-3 align-items-center">
    <form action="User.Pengukur/{{ $data->id }}" method="post">
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RA" class="col-form-label">RA </label>
                </div>
                <div class="col">
                  <input type="text" id="RA" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[1] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SA" class="col-form-label">SA </label>
                </div>
                <div class="col">
                  <input type="text" id="SA" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[2] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TA" class="col-form-label">TA</label>
                </div>
                <div class="col">
                  <input type="text" id="TA" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[3] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NA" class="col-form-label">NA </label>
                </div>
                <div class="col">
                  <input type="text" id="NA" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[4] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>


        

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RB" class="col-form-label">RB </label>
                </div>
                <div class="col">
                  <input type="text" id="RB" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[5] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SB" class="col-form-label">SB </label>
                </div>
                <div class="col">
                  <input type="text" id="SB" class="form-control" aria-describedby="help" name="data[]"value="{{ $data[6] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TB" class="col-form-label">TB </label>
                </div>
                <div class="col">
                  <input type="text" id="TB" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[7] }}"> 
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NB" class="col-form-label">NB </label>
                </div>
                <div class="col">
                  <input type="text" id="NB" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[8] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>


                

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RC" class="col-form-label">RC</label>
                </div>
                <div class="col">
                  <input type="text" id="RC" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[9] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SC" class="col-form-label">SC</label>
                </div>
                <div class="col">
                  <input type="text" id="SC" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[10] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TC" class="col-form-label">TC </label>
                </div>
                <div class="col">
                  <input type="text" id="TC" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[11] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NC" class="col-form-label">NC </label>
                </div>
                <div class="col">
                  <input type="text" id="NC" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[12] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>


                

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RD" class="col-form-label">RD </label>
                </div>
                <div class="col">
                  <input type="text" id="RD" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[13] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SD" class="col-form-label">SD </label>
                </div>
                <div class="col">
                  <input type="text" id="SD" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[14] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TD" class="col-form-label">TD</label>
                </div>
                <div class="col">
                  <input type="text" id="TD" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[15] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="ND" class="col-form-label">ND</label>
                </div>
                <div class="col">
                  <input type="text" id="ND" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[16] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        

        {{-- <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RU" class="col-form-label">RU {{ $item[16] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="RU" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SU" class="col-form-label">SU {{ $item[17] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="SU" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TU" class="col-form-label">TU {{ $item[18] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="TU" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="NU" class="col-form-label">NU {{ $item[19] }}</label>
                </div>
                <div class="col">
                  <input type="text" id="NU" class="form-control" aria-describedby="help" name="data[]">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div> --}}

        

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="RN" class="col-form-label">RN</label>
                </div>
                <div class="col">
                  <input type="text" id="RN" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[21] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="SN" class="col-form-label">SN</label>
                </div>
                <div class="col">
                  <input type="text" id="SN" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[22] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row row-cols-2" >
                <div class="col">
                  <label for="TN" class="col-form-label">TN</label>
                </div>
                <div class="col">
                  <input type="text" id="SN" class="form-control" aria-describedby="help" name="data[]" value="{{ $data[23] }}">
                </div>
                <div class="col">
                  <span id="help" class="form-text">
                    TambahKan + (+3) untuk menambah,  dan - (-3) Untuk mengurangi
                  </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-3 , mb-3">
        <button class="btn btn-primary">SIMPAN</button>
        <button class="btn btn-danger">KEMBALI</button>
    </div>
    </form>  
  </div>
</body>


@endsection
