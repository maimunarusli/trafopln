
@extends('template')
@section('content')

<style>
    #cssTable td , th
{
    text-align: center; 
    vertical-align: middle;
}

    #a
    {
        background-color: #ff7e79;
    }
    #b
    {
        background-color: #effd78
    }
    #c
    {
        background-color: #95b3d7
    }
    #d
    {
        background-color: #bfbfbf
    }
    h1{
        text-align: center;
    }


</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
<h1 style="align=center">Data Pengukuran Gardu</h1>

<table border="1" style="width: 100%" id="cssTable">
<tr>
            <th rowspan="3">NO</th>
            <th rowspan="3">No GTT</th>
            <th rowspan="3">Alamat</th>
            <th colspan="20">ARUS RMS (A)</th>
            <th rowspan="2" colspan="3">Tegangan</th>
            <th colspan="2">Analisa Beban</th>
            <th rowspan="3">Dokumentasi</th>
            <th rowspan="3" colspan="2">Aksi</th>      
    	<tr>
    		<th colspan="4">LINE A</th>
            <th colspan="4">lINE B</th>
            <th colspan="4">lINE C</th>
            <th colspan="4">lINE D</th>
            <th colspan="4">UTAMA</th>
            <th rowspan="2">Delta</th>
            <th rowspan="2">ARUS NETRAL DIBANDING <br>
             ARUS PHASE TERENDAH</th>

    	</tr>
    	<tr>
    		<td>R</td>
    		<td>S</td>
    		<td>T</td>
    		<td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R-N</td>
            <td>S-N</td>
            <td>T-N</td>
    	</tr>
        @php
            $i = 1;
        @endphp
        @foreach ($data as $item)
        <tr>
            <td>{{ $i }}</td>
            <td>{{ $item->kode }}</td>
            <td>{{ $item->alamat }}</td>
            <td id="a" >{{ $item[1] }}</td>
            <td id="b" >{{ $item[2] }}</td>
            <td id="c">{{ $item[3] }}</td>
            <td id="d">{{ $item[4] }}</td>
            
            <td id="a" >{{ $item[5] }}</td>
            <td id="b" >{{ $item[6] }}</td>
            <td id="c" >{{ $item[7] }}</td>
            <td id="d" >{{ $item[8]}}</td>

            <td id="a" >{{ $item[9] }}</td>
            <td id="b" >{{ $item[10] }}</td>
            <td id="c" >{{ $item[11] }}</td>
            <td id="d" >{{ $item[12] }}</td>

            <td id="a" >{{ $item[13] }}</td>
            <td id="b" >{{ $item[14] }}</td>
            <td id="c" >{{ $item[15] }}</td>
            <td id="d" >{{ $item[16] }}</td>

            <td id="a" >{{ $item[17] }}</td>
            <td id="b" >{{ $item[18] }}</td>
            <td id="c" >{{ $item[19] }}</td>
            <td id="d" >{{ $item[20] }}</td>

            <td id="a" >{{ $item[21] }}</td>
            <td id="b" >{{ $item[22] }}</td>
            <td id="c" >{{ $item[23] }}</td>
            <td>{{ $item->delta }}</td>
            @if ($item->arus == "ARUS NETRAL LEBIH BESAR")
                <td style="background-color: red">{{ $item->arus }}</td>
            @else
                <td>{{ $item->arus }}</td>
            @endif
            <td></td>
            <td>
                <div>
                    <i class="bi bi-pencil-square"></i>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="blue" class="bi bi-pencil-square" viewBox="0 0 16 16">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                      </svg>
                </div>
            </td>
            <td>
                <div>
                    <i class="bi bi-trash"></i>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-trash" viewBox="0 0 16 16">
                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                      </svg>
                </div>
            </td>
            
               
                </form> 
                
        </tr>
        @php
            $i++
        @endphp
        @endforeach
        
</table>


@endsection
