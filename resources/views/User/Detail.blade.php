@extends('template')
@section('content')
<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
</head>
<body>
    <table border="1" style="width: 100%">
        <tr>
            <th rowspan="3">NO</th>
            <th rowspan="3">No GTT</th>
            <th rowspan="3">Alamat</th>
            <th colspan="16">ARUS RMS (A)</th>
            <th rowspan="2" colspan="3">Tegangan</th>
            <th colspan="2">Analisa Beban</th>
            <th rowspan="2">Dokumentasi</th>
            <th rowspan="2" colspan="2">Aksi</th>      
    	<tr>
    		<th colspan="4">LINE A</th>
            <th colspan="4">lINE B</th>
            <th colspan="4">lINE C</th>
            <th colspan="4">UTAMA</th>
            <th>Delta</th>
            <th>ARUS NETRAL DIBANDING <br>
             ARUS PHASE TERENDAH</th>

    	</tr>
    	<tr>
    		<td>R</td>
    		<td>S</td>
    		<td>T</td>
    		<td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R</td>
            <td>S</td>
            <td>T</td>
            <td>N</td>
            <td>R-N</td>
            <td>S-N</td>
            <td>T-N</td>
            <td></td>
            <td>100</td>
            <td></td>
            <td></td>
            <td></td>
    	</tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <div>
        <button type="submit" class="bi bi-clipboard-plus"></button>
    </div>

</body>
</html>
@endsection
