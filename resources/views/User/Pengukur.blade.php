
@extends('template')
@section('content')

@php
    // dd();
@endphp
<style>
    #cssTable td , th
{
    text-align: center; 
    vertical-align: middle;
}

    #a
    {
        background-color: #ff7e79;
    }
    #b
    {
        background-color: #effd78
    }
    #c
    {
        background-color: #95b3d7
    }
    #d
    {
        background-color: #bfbfbf
    }


</style>

<br>
<br>
    
<table border="1" style="width: 100%" id="cssTable">
    <tr>
        <th rowspan="3">No GTT</th>
        <th rowspan="3">Alamat</th>
        <th rowspan="3">NO</th>
        <th colspan="20">ARUS RMS (A)</th>
                <th rowspan="2" colspan="3">Tegangan</th>
                <th colspan="2">Analisa Beban</th>
                <th rowspan="3">Dokumentasi</th>    
                <th rowspan="3">Aksi</th>
            <tr>
                <th colspan="4">LINE A</th>
                <th colspan="4">lINE B</th>
                <th colspan="4">lINE C</th>
                <th colspan="4">lINE D</th>
                <th colspan="4">UTAMA</th>
                <th rowspan="2">Delta Arus Phasa RMS Max-Min</th>
                <th rowspan="2">ARUS NETRAL DIBANDING <br>
                 ARUS PHASE TERENDAH</th>
    
            </tr>
            <tr>
                <td>R</td>
                <td>S</td>
                <td>T</td>
                <td>N</td>
                <td>R</td>
                <td>S</td>
                <td>T</td>
                <td>N</td>
                <td>R</td>
                <td>S</td>
                <td>T</td>
                <td>N</td>
                <td>R</td>
                <td>S</td>
                <td>T</td>
                <td>N</td>
                <td>R</td>
                <td>S</td>
                <td>T</td>
                <td>N</td>
                <td>R-N</td>
                <td>S-N</td>
                <td>T-N</td>
            </tr>
            @php
                $i = 1;
            @endphp
            <tr>
                <td rowspan="10">{{ $dataAll["gtt"]->kode }}</td>
                <td rowspan="10">{{ $dataAll["alamat"]->alamat }}</td>
            @foreach ($dataAll["trafo"] as $item)
                <td >{{ $i }}</td>
                <td id="a" >{{ $item[1] }}</td>
                <td id="b" >{{ $item[2] }}</td>
                <td id="c">{{ $item[3] }}</td>
                <td id="d">{{ $item[4] }}</td>
                
                <td id="a" >{{ $item[5] }}</td>
                <td id="b" >{{ $item[6] }}</td>
                <td id="c" >{{ $item[7] }}</td>
                <td id="d" >{{ $item[8]}}</td>

                <td id="a" >{{ $item[9] }}</td>
                <td id="b" >{{ $item[10] }}</td>
                <td id="c" >{{ $item[11] }}</td>
                <td id="d" >{{ $item[12] }}</td>

                <td id="a" >{{ $item[13] }}</td>
                <td id="b" >{{ $item[14] }}</td>
                <td id="c" >{{ $item[15] }}</td>
                <td id="d" >{{ $item[16] }}</td>
    
                <td id="a" >{{ $item[17] }}</td>
                <td id="b" >{{ $item[18] }}</td>
                <td id="c" >{{ $item[19] }}</td>
                <td id="d" >{{ $item[20] }}</td>
    
                <td id="a" >{{ $item[21] }}</td>
                <td id="b" >{{ $item[22] }}</td>
                <td id="c" >{{ $item[23] }}</td>
                <td>{{ $item->delta }}</td>
                <td>{{ $item->arus }}</td>
                <td></td>
                <td>
                    <a href="Update/{{ $dataAll['gtt']->id}}" class="btn btn-info">Edit</a>
                </td>
                
                
                   
            </td>
            </tr>
            @php
                $i++
            @endphp
            @endforeach 
    </table>
<br> 
<div class="position-relative" style="float:right ;border-radius:50px;" >
        <a href="form/{{ $dataAll['gtt']->id }}" class="btn btn-primary mb-3">+</a>
</div>
{{-- <div style="float: right">
<a href="Form"button class="btn"><i class="bi bi-plus-circle"></i>

<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" fill="blue" class="bi bi-plus-circle" viewBox="0 0 16 16">
    <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
    <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
  </svg>
</div> --}}
@endsection 
