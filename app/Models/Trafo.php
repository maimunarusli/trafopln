<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trafo extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function alamats(){
        return $this->belongsTo(Alamat::class, "alamat_id");
    }
}
