<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Alamat extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function trafos(){
        return $this->hasMany(Trafo::class, "alamat_id");
    }
    public function alamat(){
        return $this->belongsTo(Gtt::class, "Gtt_id");
    }
}
