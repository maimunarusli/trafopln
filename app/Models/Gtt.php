<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gtt extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function alamats(){
        return $this->hasMany(Alamat::class, "gtt_id");
    }
}
