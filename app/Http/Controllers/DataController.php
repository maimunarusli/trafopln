<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Gtt;
use App\Models\Trafo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Mockery\Generator\StringManipulation\Pass\Pass;

class DataController extends Controller
{
    public function dashboard(Request $request){
        $data = Gtt::join("alamats", "alamats.gtt_id", "=" , "gtts.id")
                ->join("trafos", "trafos.alamat_id", "=" , "alamats.id")
                ->get(["gtts.kode" , "alamats.alamat"]);
        return view("dashboard", compact("data"));
    }
    public function dataAdmin(Request $request){
        $data = Gtt::join("alamats", "alamats.gtt_id", "=" , "gtts.id")
                ->join("trafos", "trafos.alamat_id", "=" , "alamats.id")
                ->get(["gtts.kode" , "alamats.alamat","trafos.id", "trafos.1", "trafos.2", "trafos.3", "trafos.4", "trafos.5", "trafos.6", "trafos.7", "trafos.8", "trafos.9", "trafos.10", "trafos.11", "trafos.12", "trafos.13", "trafos.14", "trafos.15", "trafos.16", "trafos.17", "trafos.18", "trafos.19", "trafos.20", "trafos.21", "trafos.22", "trafos.23", "trafos.delta","trafos.arus"]);
                
        return view("data-admin", compact("data"));
    }

    public function cariData(Request $request){
        $cari = $request->gtt;
        $data = Gtt::all()->where("kode",$cari)->first();
        $alamat = Alamat::where("gtt_id", $data->id)->first();
        $trafos = Trafo::where("alamat_id", $alamat->id)->get();
        $dataAll = ["gtt" => $data, "alamat" => $alamat, "trafo" => $trafos];
        return view("User.Pengukur",compact("dataAll"));
    }

    public function editData(Request $request, $id){
        // dd($id);
        $data = Gtt::find($id);
        $alamat = Alamat::where("gtt_id", $data->id)->first();
        $trafos = Trafo::where("alamat_id", $alamat->id)->get();
        $dataAll = ["gtt" => $data, "alamat" => $alamat->id, "trafo" => $trafos];
        return view("Admin.Form",compact("dataAll"));
    }
    public function saveData(Request $request){
        $idTf = (int) $request->trafo_id;
        $dataA = Trafo::where("id" , $idTf+1)->get();

        // dd($request->all());

        //menampung data awal
        for($i=0 ;$i<23;$i++ ){
            $item[$i] = $dataA[0][$i+1];
        }


        $data = $request->data;
        

    
      
        // menampung nilai
        for ($i = 0;  $i<19; $i++ ){
            if($i >= 19){
            }else if ($data[$i] == ""){
                $data[$i] = 0;
            }
            if ($item[$i] == " - " || $item[$i] == ""){
                $item[$i] = 0;
            }
            if($i >= 16){
                if (substr($data[$i],0,1) == "+"){
                    $a = (int)substr($data[$i] , 1,4);
                    $b = (int)$item[$i+4];
                    
                    $data[$i] = $b + $a;
                }else if(substr($data[$i],0,1) == "-"){
                    $a = (int)substr($data[$i] , 1,4);
                    $b = (int)$item[$i+4];
                    $data[$i] = $b - $a;
                }else{
                    $b = (int)$item[$i+4];
                    $data[$i] = $b;
                }
            }else{
                if (substr($data[$i],0,1) == "+"){
                    $a = (int)substr($data[$i] , 1,4);
                    $b = (int)$item[$i];
                    
                    $data[$i] = $b + $a;
                }else if(substr($data[$i],0,1) == "-"){
                    $a = (int)substr($data[$i] , 1,4);
                    $b = (int)$item[$i];
                    $data[$i] = $b - $a;
                }else{
                    $b = (int)$item[$i];
                    $data[$i] = $b;
                }
            }
            if ($item[$i] == " - " || $item[$i] == ""){
                $ru = (int)$item[0]+(int)$item[4]+(int)$item[8]+(int)$item[12];
                $su = (int)$item[1]+(int)$item[5]+(int)$item[9]+(int)$item[13];
                $tu = (int)$item[2]+(int)$item[6]+(int)$item[10]+(int)$item[14];
                $nu = (int)$item[3]+(int)$item[7]+(int)$item[11]+(int)$item[15];
                $item[$i] = 0;
            }else{
                $ru = (int)$data[0]+(int)$data[4]+(int)$data[8]+(int)$data[12] + (int)$item[0]+(int)$item[4]+(int)$item[8]+(int)$item[12];
                $su = (int)$data[1]+(int)$data[5]+(int)$data[9]+(int)$data[13] + (int)$item[1]+(int)$item[5]+(int)$item[9]+(int)$item[13];
                $tu = (int)$data[2]+(int)$data[6]+(int)$data[10]+(int)$data[14] +(int)$item[2]+(int)$item[6]+(int)$item[10]+(int)$item[14];
                $nu = (int)$data[3]+(int)$data[7]+(int)$data[11]+(int)$data[15] + (int)$item[3]+(int)$item[7]+(int)$item[11]+(int)$item[15];
            }
    
        }
        
        $maxv = max($ru,$su,$tu);
        $minv = min($ru,$su,$tu);


         // mencari delta
         if($minv == 0){
            $delta = 0;
        }else{
            $delta = (float)((($maxv-$minv))*100/$minv);
        }

  

        // mencari arus
        if($nu > $minv){
            $arus = "ARUS NETRAL LEBIH BESAR";
        }else if ($nu == 0 && $minv == 0){
            $arus = "Nilai 0";
        }else{
            $arus = "normal";
        }

        $trafo = Trafo::create([
                "alamat_id" => $request->alamat_id,

                "1" => $data[0],
                "2" => $data[1],
                "3" => $data[2],
                "4" => $data[3],

                "5" => $data[4],
                "6" => $data[5],
                "7" => $data[6],
                "8" => $data[7],

                "9" => $data[8],
                "10" => $data[9],
                "11" => $data[10],
                "12" => $data[11],

                "13" => $data[12],
                "14" => $data[13],
                "15" => $data[14],
                "16" => $data[15],

                "17" => $ru,
                "18" => $su,
                "19" => $tu,
                "20" => $nu,

                "21" => $data[16],
                "22" => $data[17],
                "23" => $data[18],

                "delta" => round($delta, 2),
                "arus" => $arus,
        ]);

        if ($trafo){
            return redirect()->route("dashboard");
        }
    }
   public function Update($id){
    $data = Trafo::find($id);
    // dd($data);
    return view('Admin.Update', compact("data"));
   }
   public function Updatedata(Request $request, $id){
       $data = Trafo::find($id);
       $data->Update($request->all());
       return redirect()->route("User.Pengukur");
   }
   public function delete($id){
       $data = Trafo::find($id);
       $data->delete();
       return redirect()->route('dataAdmin');
   }


};

